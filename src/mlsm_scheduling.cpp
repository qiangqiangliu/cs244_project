
#include "mlsm_scheduling.h"
#include "simulator.h"
#include "scheduling_input.h"
#include <algorithm>
#include <iostream>
#include <cstdlib>
#include "main.h"
#include "hungarian.h"

MLSMScheduling::MLSMScheduling(){
  SetSchedulingAlgoType(MLSM);
}

MLSMScheduling::~MLSMScheduling(){
}



void
MLSMScheduling::Schedule(CrossbarConfig* crossbarConfig, std::vector<SchedulingInput*> schedulingInputs){
  crossbarConfig->ResetCrossbar();
  std::vector< std::vector<int> > allDesiredOutputPortIDs ;

  for (int iterSchedInput = 0; iterSchedInput < schedulingInputs.size(); iterSchedInput++){
     std::vector<int> temp = schedulingInputs[iterSchedInput]->GetDesiredOutputPortIDs();
     std::sort(temp.begin(), temp.end());

     
#if DEBUG
    for (int i = 0; i < temp.size() ; i ++) std::cout<< "input port" << iterSchedInput+1  <<" output ports " << temp[i] << std::endl ;
#endif

     std::vector<int>::iterator it;
     it = std::unique (temp.begin(), temp.end());
     temp.resize( std::distance(temp.begin(),it) ); 
     allDesiredOutputPortIDs.push_back(temp);


#if DEBUG
    for (int i = 0; i < temp.size() ; i ++) std::cout<< "input port " << iterSchedInput + 1 << " output ports " << temp[i] << " weight = " << 1 << std::endl;
#endif
  }

  Maximal( crossbarConfig,allDesiredOutputPortIDs);


  return;
}
void
MLSMScheduling::Maximal(CrossbarConfig* crossbarConfig, std::vector< std::vector <int> > outputPortsForInputs){
	// ignore weight for now
	//set node 0 as source 
	// set node 2n as destination in max flow
 	   
    std::vector< std::vector<int> > cost;
    int numPorts= crossbarConfig->GetNumPorts();

    std::vector<int> randomPerm;
    for (int i=1; i <= numPorts; i++) randomPerm.push_back(i);   
    std::random_shuffle ( randomPerm.begin(), randomPerm.end() );

    for (int inputIter = 0; inputIter < numPorts; inputIter++) {
       int inputPort= randomPerm[inputIter];
       for (int outputIter = 0; outputIter < outputPortsForInputs[inputPort-1].size(); outputIter++) {
           int outputPort= outputPortsForInputs[inputPort-1][outputIter];
           std::vector<int> unAllocatedOutputs=crossbarConfig->GetUnallocatedOutputPorts();
           if(std::find(unAllocatedOutputs.begin(),unAllocatedOutputs.end(),outputPort)!=\
			  unAllocatedOutputs.end()) {
			  
#if DEBUG
		    std::cout << " Crossbar Input port " << inputPort <<" Output port " << outputPort << std::endl;
#endif 
	                crossbarConfig->SetCrossbarLinkage(inputPort , outputPort);
	   }
       }	
    }
    return;
}



