#include "pim_scheduling.h"
#include "simulator.h"
#include "scheduling_input.h"
#include <algorithm>
#include <iostream>
#include <cstdlib>
#include "main.h"

PIMScheduling::PIMScheduling(int iterations){
  SetSchedulingAlgoType(PIM);
  d_iterations = iterations;
}

PIMScheduling::~PIMScheduling(){
}

void
PIMScheduling::Schedule(CrossbarConfig* crossbarConfig, std::vector<SchedulingInput*> schedulingInputs){
  crossbarConfig->ResetCrossbar();
  std::vector< std::vector<int> > allDesiredOutputPortIDs ;

  for (int iterSchedInput = 0; iterSchedInput < schedulingInputs.size(); iterSchedInput++){
     std::vector<int> temp = schedulingInputs[iterSchedInput]->GetDesiredOutputPortIDs();
     std::sort(temp.begin(), temp.end());
     std::vector<int>::iterator it;
     it = std::unique (temp.begin(), temp.end());
     temp.resize( std::distance(temp.begin(),it) ); 
     allDesiredOutputPortIDs.push_back(temp);
  }
  for (int iterationCount = 1; iterationCount <= d_iterations; iterationCount++){
      PimIteration( crossbarConfig, allDesiredOutputPortIDs);
  }

  return;
}

void
PIMScheduling::PimIteration(CrossbarConfig* crossbarConfig, std::vector< std::vector <int> > allDesiredOutputPortIDs){
    std::vector<int> inputsGranted;
    std::vector<int> unallocatedOutputPorts = crossbarConfig->GetUnallocatedOutputPorts();
    std::vector<int> unallocatedInputPorts = crossbarConfig->GetUnallocatedInputPorts(); 

#if DEBUG
    for (int i = 0; i < unallocatedOutputPorts.size() ; i ++){
           std::cout<< " output port after the iteration "<< unallocatedOutputPorts[i] << std::endl ;
    }
    for (int i = 0; i < unallocatedInputPorts.size() ; i ++) {
           std::cout<< " Input port after the iteration "<< unallocatedInputPorts[i] << std::endl;
    }
#endif
    for (int iterSchedOutput = 0; iterSchedOutput < unallocatedOutputPorts.size() ; iterSchedOutput++){
         // look at the requests coming for this output port
   	 int outputPortID = unallocatedOutputPorts[iterSchedOutput] ; 
    	 std::vector <int> inputRequest;
	 for (int iterInput=0; iterInput< unallocatedInputPorts.size(); iterInput++){
                // check whether this input port would request the output port
                int inputPortID = unallocatedInputPorts[iterInput];
		std::vector <int> tempOutputRequest= allDesiredOutputPortIDs[inputPortID - 1];
		if(std::find(tempOutputRequest.begin(), tempOutputRequest.end(),outputPortID)!=tempOutputRequest.end() ){
			inputRequest.push_back(inputPortID);
		}
	 }
	 //select one input at random and push to output map
	 if(inputRequest.size()){
		 int rand_generate= std::rand() % inputRequest.size();
		 inputsGranted.push_back(inputRequest[rand_generate]);
	 }
	 else{
		 inputsGranted.push_back(-1);
	 }
	 // now at the end of for loop we should have input granted
	 // for each output 
    }
    
#if DEBUG1
            std::cout << "size of inputsGranted "<< inputsGranted.size() ;
#endif

    // loop on input to remove duplicate in inputsGranted
    std::vector <int> inputsAllocated;
    // a random permutation over the output ports 
    std::vector<int> randomPerm;
    for (int i=0; i <= unallocatedOutputPorts.size(); i++) randomPerm.push_back(i);   
    std::random_shuffle ( randomPerm.begin(), randomPerm.end() );

    for (int iterOutput= 0; iterOutput < unallocatedOutputPorts.size(); iterOutput++){
	    int outputPortID= unallocatedOutputPorts[randomPerm[iterOutput]];
	    int inputPortID= inputsGranted[randomPerm[iterOutput]]; 

	    if( (std::find(inputsAllocated.begin(), inputsAllocated.end(), inputPortID) == inputsAllocated.end()) && (inputPortID>0)){
                    // valid input port ID which has not been allocated yet
		    crossbarConfig->SetCrossbarLinkage(inputPortID, outputPortID);
                    inputsAllocated.push_back(inputPortID);
#if DEBUG
		    std::cout << "Input port " << inputPortID <<"Output port" << outputPortID  << std::endl;
#endif 
	    }
   }
   return;
}
