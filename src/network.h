#ifndef NETWORK_H
#define NETWORK_H

#include "switch.h"
#include "traffic_generator.h"
#include "link.h"
#include "traffic_sink.h"
#include <vector>
#include "input_port.h"
#include "topo.h"
#include <cstddef>

class Network{

private:
  std::vector<Switch*> d_switches;
  std::vector<TrafficGenerator*> d_trafficGenerators;
  std::vector<Link*> d_links;
  std::vector<TrafficSink*> d_trafficSinks;
  Topo::TopoType d_topoType;
public:
  Network(Topo::TopoType topoType);
  ~Network();

  void AddSwitches(std::vector<Switch*> switches);
  void AddLinks(std::vector<Link*> links);
  void AddTrafficGenerators(std::vector<TrafficGenerator*> trafficGenerators);
  void AddTrafficSinks(std::vector<TrafficSink*> trafficSinks);
  std::vector<Switch*> GetSwitches();
  Switch* GetSwitchByID(int switchID);
  std::vector<TrafficGenerator*> GetTrafficGenerators();
  TrafficGenerator* GetTrafficGeneratorByID(int trafficGeneratorID);
  std::vector<TrafficSink*> GetTrafficSinks();
  TrafficSink* GetTrafficSinkByID(int trafficSinkID);

  std::vector<InputPort*> GetPath(TrafficGenerator* trafficGenerator, TrafficSink* trafficSink);
};

#endif
