#ifndef QUEUEDINPUTPORT_H
#define QUEUEDINPUTPORT_H

#include "packet.h"
#include <vector>
#include "scheduling_input.h"

class QueuedInputPort:public InputPort{

public:
   QueuedInputPort(int maxBufferSize);
   ~QueuedInputPort();

   std::vector<Packet*> GetBuffer();

   virtual void ReceivePacket();
   Packet* GetPacketForOutputPortID(int outputPortID);
   std::vector<int> GetDesiredOutputPortIDs();
   SchedulingInput* GetSchedulingInput();
private:
   std::vector<Packet*> d_buffer;
   int d_maxBufferSize;
};

#endif 
