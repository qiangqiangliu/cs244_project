#ifndef SINK_H
#define SINK_H

class Link;

class Sink{

public:
   Sink();
   virtual ~Sink();
   enum SinkType{
         TRAFFIC_SINK,
         INPUT_PORT};
   virtual void ReceivePacket() = 0;
   SinkType GetSinkType();
   Link* GetAttachedLink();
   void SetSinkType(SinkType sinkType);
   void SetAttachedLink(Link* link);
private:
   SinkType d_sinkType;
   Link* d_attachedLink;
};

#endif    
