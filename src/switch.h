#ifndef SWITCH_H
#define SWITCH_H

#include "scheduling_algo.h"
#include "input_port.h"
#include "output_port.h"
#include <cstddef>
#include <vector>
#include <algorithm>

class Switch{

public:
    enum SwitchType{
       IQ_SWITCH,
       CIOQ_SWITCH,
       OQ_SWITCH};
    Switch();
    virtual ~Switch();
    
    int GetNumPorts(); 
    SwitchType GetSwitchType();
    SchedulingAlgo* GetSchedulingAlgo(); 
    int GetSwitchID();
    void SetNumPorts(int numPorts);
    void SetSwitchType(SwitchType switchType);
    void SetSchedulingAlgo(SchedulingAlgo* schedulingAlgo);
    void SetSwitchID(int switchID);
    void AddInputPort(InputPort* inputPort);
    std::vector<InputPort*> GetInputPorts();
    InputPort* GetInputPortByID(int inputPortID);
    void AddOutputPort(OutputPort* outputPort);
    std::vector<OutputPort*> GetOutputPorts();
    OutputPort* GetOutputPortByID(int outputPortID);
    std::vector<int> GetRandomPerm();
    void SetRandomPerm();   
 
    virtual void ScheduleSwitch() = 0;
    void AdvanceSwitch();
private:
    int d_switchID;
    SchedulingAlgo* d_schedulingAlgo;
    std::vector<InputPort *> d_inputPorts;
    std::vector<OutputPort *> d_outputPorts;
    SwitchType d_switchType;
    int d_numPorts;
    std::vector<int> d_randomPerm;
};

#endif
