make clean
make
Algorithm=FIFO #FIFO,MSM,MWM,MLSM,PIM1,PIM2,PIM3,PIM4 
traffic_model=Asymmetric #(Asymmetric,Uniform}
numInstants=100000
MaxBuffer=4000

exptid=`date +%b%d_%H`
dir=results_$exptid
rm -r $dir
mkdir $dir
test1=$(seq 0.1 .1 .4)
test2=$(seq 0.45 .04 .8)
test3=$(seq 0.81 .01 1)

Switch=IQ  #CIOQ, OQ, IQ

for Algorithm in FIFO MSM MWM MLSM PIM4
do
     for traffic_model in  Asymmetric
     do
          for MaxBuffer in  2000
          do
	   	for numInstants in  50000
	   	do
	   		RESULTS=$dir/"$Algorithm"_traffic_"$traffic_model"_time_"$numInstants"_Switch_"$Switch"_MaxBuffer_"$MaxBuffer".txt
			echo "$RESULTS"
			for x in $test1
	   		do 
			    ./main $Switch $MaxBuffer $numInstants $traffic_model $Algorithm $x >> $RESULTS 
	   		    echo "$x"
			done
			for x in $test2
	   		do 
			    ./main $Switch $MaxBuffer $numInstants $traffic_model $Algorithm $x >> $RESULTS 
	   		    echo "$x"
			done

			for x in $test3
	   		do 
			    ./main $Switch $MaxBuffer $numInstants $traffic_model $Algorithm $x >> $RESULTS 
	   		    echo "$x"
			done
	   	done
	  done
     done
done


Switch=CIOQ

for Algorithm in MLSM
do
     for traffic_model in  Asymmetric
     do
          for MaxBuffer in  2000
          do
	   	for numInstants in  50000 
	   	do
	   		RESULTS=$dir/"$Algorithm"_traffic_"$traffic_model"_time_"$numInstants"_Switch_"$Switch"_MaxBuffer_"$MaxBuffer".txt
			echo "$RESULTS"
			for x in $test1
	   		do 
			    ./main $Switch $MaxBuffer $numInstants $traffic_model $Algorithm $x >> $RESULTS 
	   		    echo "$x"
			done
			for x in $test2
	   		do 
			    ./main $Switch $MaxBuffer $numInstants $traffic_model $Algorithm $x >> $RESULTS 
	   		    echo "$x"
			done

			for x in $test3
	   		do 
			    ./main $Switch $MaxBuffer $numInstants $traffic_model $Algorithm $x >> $RESULTS 
	   		    echo "$x"
			done
	   	done
	  done
     done
done

