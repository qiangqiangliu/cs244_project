
#include "msm_scheduling.h"
#include "simulator.h"
#include "scheduling_input.h"
#include <algorithm>
#include <iostream>
#include <cstdlib>
#include "main.h"
#include "hungarian.h"
MSMScheduling::MSMScheduling(){
  SetSchedulingAlgoType(MSM);
}

MSMScheduling::~MSMScheduling(){
}



void
MSMScheduling::Schedule(CrossbarConfig* crossbarConfig, std::vector<SchedulingInput*> schedulingInputs){
  crossbarConfig->ResetCrossbar();
  std::vector< std::vector<int> > allDesiredOutputPortIDs ;
  std::vector< std::vector<int> > allDesiredOutputPortIDWeights ;

  for (int iterSchedInput = 0; iterSchedInput < schedulingInputs.size(); iterSchedInput++){
     std::vector<int> temp = schedulingInputs[iterSchedInput]->GetDesiredOutputPortIDs();
     std::sort(temp.begin(), temp.end());

     
#if DEBUG
    for (int i = 0; i < temp.size() ; i ++) std::cout<< "input port" << iterSchedInput+1  <<" output ports " << temp[i] << std::endl ;
#endif

     std::vector<int>::iterator it;
     it = std::unique (temp.begin(), temp.end());
     temp.resize( std::distance(temp.begin(),it) ); 
     allDesiredOutputPortIDs.push_back(temp);


#if DEBUG
    for (int i = 0; i < temp.size() ; i ++) std::cout<< "input port " << iterSchedInput + 1 << " output ports " << temp[i] << " weight = " << 1 << std::endl;
#endif
  }

  MaxFlow( crossbarConfig,allDesiredOutputPortIDs);


  return;
}
void
MSMScheduling::MaxFlow(CrossbarConfig* crossbarConfig, std::vector< std::vector <int> > outputPortsForInputs){
	// ignore weight for now
	//set node 0 as source 
	// set node 2n as destination in max flow
 	   
    std::vector< std::vector<int> > cost;
    int numPorts= crossbarConfig->GetNumPorts();

       for (int inputPort = 0; inputPort < outputPortsForInputs.size(); inputPort++) {
	          cost.push_back( std::vector<int> (numPorts, 0)); // Add an empty row
       }


       for (int inputPort = 0; inputPort < numPorts; inputPort++) {
       		for (int outputIter = 0; outputIter < outputPortsForInputs[inputPort].size(); outputIter++) {
			int outputPort= outputPortsForInputs[inputPort][outputIter] - 1 ;
	                cost[inputPort][outputPort]=1 ; // Add an empty row
       		}
       }


#if DEBUG
     for (int i=0;i<numPorts;i++){
	     for (int j=0;j<numPorts ;j++){
		     if(cost[i][j]>0)
		     std::cout << " mwm i =" << i <<"j= " << j <<"cost" << cost[i][j] << std::endl;

	     }
     }
#endif

	Hungarian* h = new Hungarian();
	h->SetValues( numPorts , cost );
        int x=(h->MaxWeight());

#if DEBUG
        std::cout << "Max Weight Bipartite matching = " << x << std::endl ;	
#endif
	std::vector<int> OutputMapping=h->ReturnY();
#if DEBUG
	std::cout << "Size of OutputMapping" << OutputMapping.size() << std::endl;
#endif
	for (int inputPort=1; inputPort<= numPorts; inputPort++ ){
    	     int outputPort=OutputMapping[inputPort-1]+1;
	     crossbarConfig->SetCrossbarLinkage(inputPort, outputPort);
	}

  return;
}



