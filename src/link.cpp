#include "link.h"
#include "link_entry.h"
#include "simulator.h"

Link::Link(Source* source, Sink* sink){
  d_source = source;
  d_sink = sink;
}

Link::~Link(){
}

Source*
Link::GetSource(){
   return d_source;
}

Sink*
Link::GetSink(){
   return d_sink;
}

void 
Link::WritePacket(Packet* packet){
   LinkEntry* newLinkEntry = new LinkEntry(packet, Simulator::GetTime());
   d_linkEntries.push_back(newLinkEntry);
}
 
Packet*
Link::ReadPacket(){
   int desiredWriteTime = Simulator::GetTime() - 1;
   Packet* desiredPacket = NULL;
   for(int iterLinkEntry = 0; iterLinkEntry < d_linkEntries.size(); iterLinkEntry++){
      if(d_linkEntries[iterLinkEntry]->GetWriteTime() == desiredWriteTime){
          desiredPacket = d_linkEntries[iterLinkEntry]->GetPacketOnLink();
          d_linkEntries.erase(d_linkEntries.begin() + iterLinkEntry);
          break;
      }
   }
   return desiredPacket;
} 
