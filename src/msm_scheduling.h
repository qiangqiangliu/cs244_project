

#ifndef MSMSCHEDULING_H
#define MSMSCHEDULING_H

#include "scheduling_algo.h"
#include "scheduling_input.h"
#include "crossbar_config.h"

class MSMScheduling:public SchedulingAlgo{

public:
   MSMScheduling();
   ~MSMScheduling();
   void MaxFlow(CrossbarConfig* crossbarConfig, std::vector< std::vector<int> > allOutput );
   virtual void Schedule(CrossbarConfig* crossbarConfig, std::vector<SchedulingInput*> schedulingInputs);
};

#endif

