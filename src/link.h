#ifndef LINK_H
#define LINK_H

#include "source.h"
#include "sink.h"
#include "packet.h"
#include "link_entry.h"

class Link{

public:
   Link(Source* source, Sink* sink);
   ~Link();

   void SetSource(Source* source);
   void SetSink(Sink* sink);
   Source* GetSource();
   Sink* GetSink();

   void WritePacket(Packet* packet);
   Packet* ReadPacket();
private:
   Source* d_source;
   Sink* d_sink;
   std::vector<LinkEntry*> d_linkEntries;
};

#endif  
   
