#include "packet.h"
#include "simulator.h"

Packet::Packet(){
  d_hopCount = 0;
  d_packetNo = Simulator::GetPacketNo();
  d_generationTime = Simulator::GetTime();
}

Packet::~Packet(){
  for(int i =0; i < d_path.size(); i++){
     delete d_path[i];
  }

}

void
Packet::SetPath(std::vector<PathEntry*> pathEntries){
   d_path = pathEntries;
}

void 
Packet::SetTrafficGeneratorID(int trafficGeneratorID){
   d_trafficGeneratorID = trafficGeneratorID;
}

void
Packet::SetTrafficSinkID(int trafficSinkID){
   d_trafficSinkID = trafficSinkID;
}

void
Packet::UpdateHopCount(){
  d_hopCount = d_hopCount + 1;
  if (d_hopCount == d_path.size() + 1){
     // reached the final traffic sink
     d_serviceTime = Simulator::GetTime();
  }
  else{
     // reached a new input port -> update the corresponding path entry
     d_path[d_hopCount -1]->SetArrivalTime(Simulator::GetTime());
  }   
}

void
Packet::SetNextOutputPortID(){
   // find next output port ID;
   OutputPort* nextOutputPort; 
   std::vector<PathEntry*> path = GetPath();
   int currHopCount = GetHopCount();
   if (currHopCount < path.size()){
        // next hop is also a switch
       InputPort* nextInputPort = path[currHopCount + 1]->GetInputPort();
       nextOutputPort = (OutputPort*) nextInputPort->GetAttachedLink()->GetSource();
   }
   else{
        // next hop is the traffic sink
        Network* network = Simulator::GetNetworkPtr();
        TrafficSink* trafficSink = network->GetTrafficSinkByID(GetTrafficSinkID());
        nextOutputPort = (OutputPort*) trafficSink->GetAttachedLink()->GetSource();
   }
   d_nextOutputPortID = nextOutputPort->GetOutputPortID();
}

std::vector<PathEntry*>
Packet::GetPath(){
   return d_path;
}
 
int 
Packet::GetPacketNo(){
   return d_packetNo;
}

int 
Packet::GetTrafficGeneratorID(){
   return d_trafficGeneratorID;
}

int 
Packet::GetTrafficSinkID(){
   return d_trafficSinkID;
}

int 
Packet::GetHopCount(){
   return d_hopCount;
}

int 
Packet::GetGenerationTime(){
   return d_generationTime;
}

int 
Packet::GetServiceTime(){
   return d_serviceTime;
}

int 
Packet::GetNextOutputPortID(){ 
   return d_nextOutputPortID;
}



