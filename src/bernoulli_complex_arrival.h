#ifndef BERNOULLICOMPLEXARRIVAL_H
#define BERNOULLICOMPLEXARRIVAL_H

#include "arrival_model.h"

class BernoulliComplexArrival:public ArrivalModel{
 
public:
   BernoulliComplexArrival(double load, std::vector<double> trafficMatrix);
   ~BernoulliComplexArrival();
 
   virtual std::vector<int> GetDestinationIDs();
   void SetLoad(double load);

private:
   double d_load;
   std::vector<double> d_trafficMatrix;
};

#endif   

