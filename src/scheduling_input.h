#ifndef SCHEDULINGINPUT_H
#define SCHEDULINGINPUT_H

#include <vector>

class SchedulingInput{

public:
   SchedulingInput();
   ~SchedulingInput();
   
   void SetInputPortID(int inputPortID);
   void SetDesiredOutputPortIDs(std::vector<int> desiredOutputPortIDs);
   int GetInputPortID();
   std::vector<int> GetDesiredOutputPortIDs();
    

   void MapSchedulingInput(std::vector<int> randomPerm);
   int Map(std::vector<int> randomPerm, int actualPortID);
private:
   int d_inputPortID;
   std::vector<int> d_desiredOutputPortIDs;
};

#endif

