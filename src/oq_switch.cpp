#include "oq_switch.h"
#include "non_queued_input_port.h"
#include "queued_output_port.h"

OQSwitch::OQSwitch(int numPorts, int maxBufferSize){
   SetSwitchType(Switch::OQ_SWITCH);
   SetNumPorts(numPorts);
   for (int portID = 1; portID <= numPorts; portID++){
       NonQueuedInputPort* newInputPort = new NonQueuedInputPort();
       newInputPort->SetInputPortID(portID);
       newInputPort->SetSwitch((Switch*) this);
       AddInputPort((InputPort*) newInputPort);

       QueuedOutputPort* newOutputPort = new QueuedOutputPort(maxBufferSize); 
       newOutputPort->SetOutputPortID(portID);
       newOutputPort->SetSwitch((Switch*) this);
       AddOutputPort((OutputPort*) newOutputPort);
   }
}

OQSwitch::~OQSwitch(){
}

void
OQSwitch::ScheduleSwitch(){
}

