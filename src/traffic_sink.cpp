#include "traffic_sink.h"
#include "packet.h"
#include "link.h"
#include "simulator.h"
#include <iostream>

TrafficSink::TrafficSink(){
   d_totalPackets = 0;
   d_averageDelay = 0.0;
}

TrafficSink::~TrafficSink(){
  for(int iterPacket =0; iterPacket < d_buffer.size(); iterPacket++){
     delete d_buffer[iterPacket];
  }
}  

void 
TrafficSink::SetTrafficSinkID(int trafficSinkID){
   d_trafficSinkID = trafficSinkID;
}

int
TrafficSink::GetTrafficSinkID(){
   return d_trafficSinkID;
}

std::vector<Packet*> 
TrafficSink::GetBuffer(){
   return d_buffer;
}

void 
TrafficSink::ReceivePacket(){
   Packet* packet = GetAttachedLink()->ReadPacket();
   if (packet != NULL){
      packet->UpdateHopCount();
      //Add it to the buffer
      d_buffer.push_back(packet);
      //update counters
      double newDelay = Simulator::GetTime() - packet->GetGenerationTime();
      double totalDelay = (double) d_totalPackets*d_averageDelay + newDelay;
      d_totalPackets++;
      d_averageDelay = totalDelay/d_totalPackets;
      if(packet->GetTrafficSinkID() != GetTrafficSinkID()){
          std::cout << " WRONG TRAFFIC SINK "<<std::endl;
      }
//      if(GetTrafficSinkID() == 1)
  //        std::cout << "average delay = "<<d_averageDelay<<" with newDelay = "<<newDelay<<" and total delay = "<<totalDelay<<" total packets = "<<d_totalPackets<< std::endl;
   }
   return;
}

int
TrafficSink::GetTotalPackets(){
   return d_totalPackets;
}

double
TrafficSink::GetAverageDelay(){
   return d_averageDelay;
}

void 
TrafficSink::AdvanceTrafficSink(){
   ReceivePacket();
}
