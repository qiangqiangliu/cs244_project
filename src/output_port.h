#ifndef OUTPUTPORT_H
#define OUTPUTPORT_H

#include "source.h"

class Switch;

class OutputPort:public Source{

public:
    enum OutputPortType{
       QUEUED_OUTPUT_PORT,
       NON_QUEUED_OUTPUT_PORT};
    OutputPort();
    virtual ~OutputPort();
    
    void SetOutputPortType(OutputPortType outputPortType);
    void SetOutputPortID(int outputPortID);
    void SetSwitch(Switch* attachedSwitch);
    int GetOutputPortID();
    Switch* GetSwitch();
    OutputPortType GetOutputPortType();

    virtual void SendPacket() = 0;
private:
    OutputPortType d_outputPortType;
    Switch* d_switch;
    int d_outputPortID;
};

#endif
