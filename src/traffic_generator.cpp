#include "traffic_generator.h"
#include "packet.h"
#include "network.h"
#include "traffic_sink.h"
#include "input_port.h"
#include "path_entry.h"
#include "simulator.h"

TrafficGenerator::TrafficGenerator(){
    SetSourceType(TRAFFIC_GENERATOR);
}

TrafficGenerator::~TrafficGenerator(){
   for(int i = 0; i< d_buffer.size(); i++){
      delete d_buffer[i];
   }
   delete d_arrivalModel;
}

void
TrafficGenerator::SetTrafficGeneratorID(int trafficGeneratorID){
   d_trafficGeneratorID = trafficGeneratorID;
}

void
TrafficGenerator::SetArrivalModel(ArrivalModel* arrivalModel){
   d_arrivalModel = arrivalModel;
}

int
TrafficGenerator::GetTrafficGeneratorID(){
   return d_trafficGeneratorID;
}

ArrivalModel*
TrafficGenerator::GetArrivalModel(){
   return d_arrivalModel;
}

Packet*
TrafficGenerator::FormPacket(int trafficSinkID){
   Packet* packet = new Packet();
   Network* network = Simulator::GetNetworkPtr();
   
   int trafficGeneratorID = GetTrafficGeneratorID();
   packet->SetTrafficGeneratorID(trafficGeneratorID);
   packet->SetTrafficSinkID(trafficSinkID);

   TrafficSink* trafficSink = network->GetTrafficSinkByID(trafficSinkID);
   std::vector<InputPort*> inputPortsOnPath = network->GetPath(this, trafficSink);
   std::vector<PathEntry*> path;
   for(int iterInputPort = 0; iterInputPort < inputPortsOnPath.size(); iterInputPort++){
      PathEntry* newPathEntry = new PathEntry();
      newPathEntry->SetInputPort(inputPortsOnPath[iterInputPort]);
      path.push_back(newPathEntry);
   }
    
   packet->SetPath(path);
   return packet;
}

void
TrafficGenerator::SendPacket(){
    std::vector<int> destinationIDs = d_arrivalModel->GetDestinationIDs();
    for(int iterDestination = 0; iterDestination < destinationIDs.size(); iterDestination++){
        Packet* packet = FormPacket(destinationIDs[iterDestination]);
        d_buffer.push_back(packet);
    }
    if (d_buffer.size() > 0){
        GetAttachedLink()->WritePacket(d_buffer[0]);
        d_buffer.erase(d_buffer.begin());
    }
    return;
}

void
TrafficGenerator::AdvanceTrafficGenerator(){
    SendPacket();
    return;
}

