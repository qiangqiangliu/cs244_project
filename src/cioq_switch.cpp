#include "cioq_switch.h"
#include "queued_input_port.h"
#include "queued_output_port.h"
#include "crossbar_config.h"
#include "input_port.h"
#include "output_port.h"


CIOQSwitch::CIOQSwitch(int numPorts, int maxBufferSize, int speedup){
   SetSwitchType(Switch::CIOQ_SWITCH);
   SetNumPorts(numPorts);
   for (int portID = 1; portID <= numPorts; portID++){
       QueuedInputPort* newInputPort = new QueuedInputPort(maxBufferSize);
       newInputPort->SetInputPortID(portID);
       newInputPort->SetSwitch((Switch*) this);
       AddInputPort((InputPort*) newInputPort);

       QueuedOutputPort* newOutputPort = new QueuedOutputPort(maxBufferSize); 
       newOutputPort->SetOutputPortID(portID);
       newOutputPort->SetSwitch((Switch*) this);
       AddOutputPort((OutputPort*) newOutputPort);
   }
   d_crossbarConfig = new CrossbarConfig(numPorts);
   d_speedup = speedup;
}

CIOQSwitch::~CIOQSwitch(){
   delete d_crossbarConfig;
}

CrossbarConfig*
CIOQSwitch::GetCrossbarConfig(){
   return d_crossbarConfig;
}

void
CIOQSwitch::SetCrossbarConfig(CrossbarConfig* crossbarConfig){
   d_crossbarConfig = crossbarConfig;
}

void
CIOQSwitch::ScheduleSwitch(){
   
   for (int speedupCount = 1; speedupCount <= d_speedup; speedupCount++){
      // get scheduling inputs
      SetRandomPerm();
      std::vector<SchedulingInput*> schedulingInputs;
      std::vector<InputPort*> inputPorts = GetInputPorts();
      std::vector<int> randomPerm = GetRandomPerm();
      for (int iterInputPort = 0; iterInputPort < inputPorts.size(); iterInputPort++){
         QueuedInputPort* currentInputPort = (QueuedInputPort*)(inputPorts[iterInputPort]);
         SchedulingInput* currentSchedulingInput = currentInputPort->GetSchedulingInput();
         currentSchedulingInput->MapSchedulingInput(randomPerm);
         schedulingInputs.push_back(currentSchedulingInput);
      }
  
      std::vector<SchedulingInput*> shuffledSchedulingInputs(schedulingInputs.size(), NULL);
      for (int iterSchedInput = 0; iterSchedInput < schedulingInputs.size(); iterSchedInput++){
         shuffledSchedulingInputs[randomPerm[iterSchedInput]] = schedulingInputs[iterSchedInput];
      }

      // run scheduling algo
      GetSchedulingAlgo()->Schedule(d_crossbarConfig, shuffledSchedulingInputs);
      d_crossbarConfig->DemapCrossbar(randomPerm);

      // copy data from inputs to outputs
      for (int outputPortID = 1; outputPortID <= GetNumPorts(); outputPortID++){
          if( d_crossbarConfig->QueryCrossbar(outputPortID) > 0){
               // valid input connected to the output
               int inputPortID = d_crossbarConfig->QueryCrossbar(outputPortID);
               InputPort* currentInputPort = GetInputPortByID(inputPortID);
               OutputPort* currentOutputPort = GetOutputPortByID(outputPortID);
               Packet* packet = ((QueuedInputPort *) currentInputPort)->GetPacketForOutputPortID(outputPortID);
               ((QueuedOutputPort *) currentOutputPort)->AddPacketToBuffer(packet);
          }
      }
   }
}


