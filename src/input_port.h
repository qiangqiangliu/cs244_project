#ifndef INPUTPORT_H
#define INPUTPORT_H

#include "sink.h"

class Switch;

class InputPort:public Sink{

public:
    enum InputPortType{
        QUEUED_INPUT_PORT,
        NON_QUEUED_INPUT_PORT};
    InputPort();
    virtual ~InputPort();

    virtual void ReceivePacket() = 0; 

    Switch* GetSwitch();
    int GetInputPortID();
    InputPortType GetInputPortType();
    void SetSwitch(Switch* attachedSwitch);
    void SetInputPortID(int inputPortID);
    void SetInputPortType(InputPortType inputPortType);
private:
    InputPortType d_inputPortType;
    Switch* d_switch;
    int d_inputPortID;
};
#endif
   

