
#include "msm_scheduling_shuffle.h"
#include "simulator.h"
#include "scheduling_input.h"
#include <algorithm>
#include <iostream>
#include <cstdlib>
#include "main.h"
#include "hungarian.h"
MSMScheduling::MSMScheduling(){
  SetSchedulingAlgoType(MSM);
}

MSMScheduling::~MSMScheduling(){
}



void
MSMScheduling::Schedule(CrossbarConfig* crossbarConfig, std::vector<SchedulingInput*> schedulingInputs){
  crossbarConfig->ResetCrossbar();
  std::vector< std::vector<int> > allDesiredOutputPortIDs ;
  std::vector< std::vector<int> > allDesiredOutputPortIDWeights ;

  for (int iterSchedInput = 0; iterSchedInput < schedulingInputs.size(); iterSchedInput++){
     std::vector<int> temp = schedulingInputs[iterSchedInput]->GetDesiredOutputPortIDs();
     std::sort(temp.begin(), temp.end());

     
#if DEBUG
    for (int i = 0; i < temp.size() ; i ++) std::cout<< "input port" << iterSchedInput+1  <<" output ports " << temp[i] << std::endl ;
#endif

     std::vector<int>::iterator it;
     it = std::unique (temp.begin(), temp.end());
     temp.resize( std::distance(temp.begin(),it) ); 
     allDesiredOutputPortIDs.push_back(temp);


#if DEBUG
    for (int i = 0; i < temp.size() ; i ++) std::cout<< "input port " << iterSchedInput + 1 << " output ports " << temp[i] << " weight = " << 1 << std::endl;
#endif
  }

  MaxFlow( crossbarConfig,allDesiredOutputPortIDs);


  return;
}
void
MSMScheduling::MaxFlow(CrossbarConfig* crossbarConfig, std::vector< std::vector <int> > outputPortsForInputs){
	// ignore weight for now
	//set node 0 as source 
	// set node 2n as destination in max flow
 	   
    std::vector< std::vector<int> > cost;
    std::vector<int>  input_shuffle;
    std::vector<int>  output_shuffle;
    int numPorts= crossbarConfig->GetNumPorts();

       for (int inputPort = 0; inputPort < outputPortsForInputs.size(); inputPort++) {
	          cost.push_back( std::vector<int> (numPorts, 0)); // Add an empty row
		  input_shuffle.push_back(inputPort );
		  output_shuffle.push_back(inputPort );
       }
       std::random_shuffle ( input_shuffle.begin(), input_shuffle.end() );
       std::random_shuffle ( output_shuffle.begin(), output_shuffle.end() );

       for (int inputPort = 0; inputPort < numPorts; inputPort++) {
       		for (int outputIter = 0; outputIter < outputPortsForInputs[inputPort].size(); outputIter++) {
			int outputPort= outputPortsForInputs[inputPort][outputIter] - 1 ;
			int inputPortnew ;
			//shuffle 
			inputPortnew =MapRandPerm( inputPort,input_shuffle);
			outputPort =MapRandPerm( outputPort, output_shuffle);

	                cost[inputPortnew][outputPort]=1 ; // Add an empty row
       		}
       }


#if DEBUG
     for (int i=0;i<numPorts;i++){
	     for (int j=0;j<numPorts ;j++){
		     if(cost[i][j]>0)
		     std::cout << " mwm i =" << i <<"j= " << j <<"cost" << cost[i][j] << std::endl;

	     }
     }
#endif

	Hungarian* h = new Hungarian();
	h->SetValues( numPorts , cost );
        int x=(h->MaxWeight());

#if DEBUG
        std::cout << "Max Weight Bipartite matching = " << x << std::endl ;	
#endif

	std::vector<int> OutputMapping=h->ReturnY();

#if DEBUG
	std::cout << "Size of OutputMapping" << OutputMapping.size() << std::endl;
#endif
	for (int inputPort=1; inputPort<= numPorts; inputPort++ ){
    	     int outputPort=OutputMapping[inputPort-1]+1;
	     int inputPortActual=-1, outputPortActual=-1;
	     inputPortActual= DeMapRandPerm(inputPort-1, input_shuffle);
	     outputPortActual= DeMapRandPerm(outputPort-1, output_shuffle);
	     crossbarConfig->SetCrossbarLinkage(inputPortActual+1, outputPortActual+1);
	}

  return;
}



int
MSMScheduling::MapRandPerm( int inputPort, std::vector<int> input_shuffle){
return input_shuffle[inputPort];
}


int
MSMScheduling::DeMapRandPerm( int inputPort, std::vector<int> input_shuffle){
	     int inputPortActual=-1;
	     std::vector<int>::iterator it;
	     it= std::find( input_shuffle.begin(), input_shuffle.end(),inputPort);
             inputPortActual= std::distance(input_shuffle.begin(),it)             ;
	     return inputPortActual;
}
