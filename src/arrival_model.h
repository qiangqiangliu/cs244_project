#ifndef ARRIVALMODEL_H
#define ARRIVALMODEL_H

#include <vector>

class ArrivalModel{

public:
   enum ArrivalType {
      ARRIVAL_BERNOULLI_SIMPLE,
      ARRIVAL_BERNOULLI_COMPLEX
   };
   ArrivalModel();
   virtual ~ArrivalModel();

   virtual std::vector<int> GetDestinationIDs() = 0;
   
   ArrivalType GetArrivalType();
   void SetArrivalType(ArrivalType arrivalType);
private:
   ArrivalType d_arrivalType;
};

#endif

