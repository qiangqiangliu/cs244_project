#ifndef SIMPLETOPO_H
#define SIMPLETOPO_H

#include "network.h"
#include "switch.h"
#include <vector>

class SimpleTopo:public Topo{

public:
  SimpleTopo(int numPorts);
  ~SimpleTopo();

  virtual Network* CreateNetwork(Switch::SwitchType switchType, int maxBufferSize);
  virtual int GetNumTrafficGenerators();
  virtual int GetNumTrafficSinks();
  virtual int GetNumSwitches();
private:
  int d_numPorts;
};

#endif
