#include "source.h"

Source::Source(){
}

Source::~Source(){
}

Link*
Source::GetAttachedLink(){
   return d_attachedLink;
}

Source::SourceType
Source::GetSourceType(){
   return d_sourceType;
}

void
Source::SetAttachedLink(Link* link){
   d_attachedLink = link;
}

void
Source::SetSourceType(SourceType sourceType){
   d_sourceType = sourceType;
}
   
