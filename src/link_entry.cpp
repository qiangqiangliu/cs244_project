#include "link_entry.h"

LinkEntry::LinkEntry(Packet* packet, int time){
   d_packetOnLink = packet;
   d_writeTime = time;
}

LinkEntry::~LinkEntry(){
}
   
int
LinkEntry::GetWriteTime(){
   return d_writeTime;
}

Packet*
LinkEntry::GetPacketOnLink(){
   return d_packetOnLink;
}
