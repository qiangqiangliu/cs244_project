#include "fifo_scheduling.h"
#include "simulator.h"
#include "scheduling_input.h"

FIFOScheduling::FIFOScheduling(){
  SetSchedulingAlgoType(FIFO);
}

FIFOScheduling::~FIFOScheduling(){
}

void
FIFOScheduling::Schedule(CrossbarConfig* crossbarConfig, std::vector<SchedulingInput*> schedulingInputs){
  crossbarConfig->ResetCrossbar();
  for (int iterSchedInput = 0; iterSchedInput < schedulingInputs.size(); iterSchedInput++){
     int inputPortID = schedulingInputs[iterSchedInput]->GetInputPortID();  
     std::vector<int> allOutputPortIDs = schedulingInputs[iterSchedInput]->GetDesiredOutputPortIDs();
     if (allOutputPortIDs.size() > 0){
         int desiredOutputPortID = allOutputPortIDs[0];
         crossbarConfig->SetCrossbarLinkage(inputPortID, desiredOutputPortID);
     }
  }
  return;
}

