#include "main.h"
#include "switch.h"

#include "simulator.h"
#include "switch.h"
#include "arrival_model.h"
#include "bernoulli_simple_arrival.h"
#include "bernoulli_complex_arrival.h"
#include "scheduling_algo.h"
#include "pim_scheduling.h"
#include "fifo_scheduling.h"
#include "mwm_scheduling.h"
#include "msm_scheduling.h"
#include "mlsm_scheduling.h"
#include "topo.h"
#include "simple_topo.h"
#include "queued_input_port.h"
#include "packet.h"
#include <cstdlib>
#include <ctime>
#include <cstdio>
#include <iostream>
#include <numeric>
#include <vector>
#include <algorithm>

int main(int argc,char* argv[]){
  std::srand(1);
  
  Switch::SwitchType switchType;
  std::string cioq = "CIOQ"; 
  std::string iq = "IQ"; 
  std::string oq = "OQ"; 
  if (argv[1] == cioq){
       switchType = Switch::CIOQ_SWITCH;
  }
  else if (argv[1] == iq){
       switchType = Switch::IQ_SWITCH;
  }
  else if (argv[1] == oq){
       switchType = Switch::OQ_SWITCH;
  }
  else {
       std::cout << "ERROR Switch" << std::endl;
  }

  int maxBufferSize = atoi(argv[2]);
  
  int timeInstants = atoi(argv[3]);
     
  double load = atof(argv[6]);
 
  SimpleTopo* topoPtr;
  std::vector<std::vector<double> > trafficMatrix;
  std::vector<ArrivalModel*> arrivalModels;
  int numPorts;
  int iterTrafficGen;
  std::string uniform = "Uniform"; 
  std::string asymmetric = "Asymmetric"; 
  if (argv[4] == asymmetric){
        //set num ports 
       numPorts = 3;
       // set topo
       topoPtr = new SimpleTopo(numPorts);
       // set traffic matrix
       double tempArray1[] = {0.5, 0.5, 0};
       std::vector<double> tempVector1 (tempArray1, tempArray1 + numPorts);
       trafficMatrix.push_back(tempVector1);
       double tempArray2[] = {0.5, 0, 0};
       std::vector<double> tempVector2 (tempArray2, tempArray2 + numPorts);
       trafficMatrix.push_back(tempVector2);
       double tempArray3[] = {0, 0.5, 0};
       std::vector<double> tempVector3 (tempArray3, tempArray3 + numPorts);
       trafficMatrix.push_back(tempVector3);
       // set arrival models
       for (iterTrafficGen = 0; iterTrafficGen < topoPtr->GetNumTrafficGenerators(); iterTrafficGen++){
           BernoulliComplexArrival* newArrivalModel = new BernoulliComplexArrival(load, trafficMatrix[iterTrafficGen]);
          // std::cout<<trafficMatrix[iterTrafficGen][0]<<" " <<trafficMatrix[iterTrafficGen][1]<<" " <<trafficMatrix[iterTrafficGen][2]<<" " <<std::endl;
           arrivalModels.push_back(newArrivalModel);
       }
  }
  else if (argv[4] == uniform){
       //set num ports 
       numPorts = 3;
       // set topo
       topoPtr = new SimpleTopo(numPorts);
       // set arrival models
       for (iterTrafficGen = 0; iterTrafficGen < topoPtr->GetNumTrafficGenerators(); iterTrafficGen++){
           BernoulliSimpleArrival* newArrivalModel = new BernoulliSimpleArrival(load);
           arrivalModels.push_back(newArrivalModel);
       }
  }
  else{
       std::cout<<"ERROR traffic model" << std::endl;
  }
  
  std::vector<SchedulingAlgo*> schedulingAlgos;
  int iterSwitch;
  std::string fifo = "FIFO";
  std::string msm = "MSM";
  std::string mwm = "MWM";
  std::string mlsm = "MLSM";
  std::string pim1 = "PIM1";
  std::string pim2 = "PIM2";
  std::string pim3 = "PIM3";
  std::string pim4 = "PIM4";
  if (argv[5] == fifo){
       for (iterSwitch = 0; iterSwitch < topoPtr->GetNumSwitches(); iterSwitch++){
          FIFOScheduling* newSchedulingAlgo = new FIFOScheduling();
          schedulingAlgos.push_back(newSchedulingAlgo);
       }
  }
  else if (argv[5] == msm){
       for (iterSwitch = 0; iterSwitch < topoPtr->GetNumSwitches(); iterSwitch++){
          MSMScheduling* newSchedulingAlgo = new MSMScheduling();
          schedulingAlgos.push_back(newSchedulingAlgo);
       }
  }
  else if (argv[5] == mwm){
       for (iterSwitch = 0; iterSwitch < topoPtr->GetNumSwitches(); iterSwitch++){
          MWMScheduling* newSchedulingAlgo = new MWMScheduling();
          schedulingAlgos.push_back(newSchedulingAlgo);
       }
  }
  else if (argv[5] == mlsm){
       for (iterSwitch = 0; iterSwitch < topoPtr->GetNumSwitches(); iterSwitch++){
          MLSMScheduling* newSchedulingAlgo = new MLSMScheduling();
          schedulingAlgos.push_back(newSchedulingAlgo);
       }
  }
  else if (argv[5] == pim1){
       for (iterSwitch = 0; iterSwitch < topoPtr->GetNumSwitches(); iterSwitch++){
          PIMScheduling* newSchedulingAlgo = new PIMScheduling(1);
          schedulingAlgos.push_back(newSchedulingAlgo);
       }
  }
  else if (argv[5] == pim2){
       for (iterSwitch = 0; iterSwitch < topoPtr->GetNumSwitches(); iterSwitch++){
          PIMScheduling* newSchedulingAlgo = new PIMScheduling(2);
          schedulingAlgos.push_back(newSchedulingAlgo);
       }
  }
  else if (argv[5] == pim3){
       for (iterSwitch = 0; iterSwitch < topoPtr->GetNumSwitches(); iterSwitch++){
          PIMScheduling* newSchedulingAlgo = new PIMScheduling(3);
          schedulingAlgos.push_back(newSchedulingAlgo);
       }
  }
  else if (argv[5] == pim4){
       for (iterSwitch = 0; iterSwitch < topoPtr->GetNumSwitches(); iterSwitch++){
          PIMScheduling* newSchedulingAlgo = new PIMScheduling(4);
          schedulingAlgos.push_back(newSchedulingAlgo);
       }
  }
  else{
       std::cout << "ERROR algo" << std::endl;
  }     
  // start simulator
  Simulator* simulator = new Simulator(arrivalModels, schedulingAlgos, switchType, (Topo*) topoPtr, maxBufferSize);

  // run simulator
  for (int simTime = 0; simTime < timeInstants; simTime++){
     simulator->AdvanceTimeInstant();
  }

  // analyze average delay 
  Network* network = simulator->GetNetworkPtr();
  std::vector<TrafficSink*> trafficSinks = network->GetTrafficSinks();
     
  double cumulativeDelay = 0;
  int cumulativePackets = 0;
  for(int iterTrafficSink = 0; iterTrafficSink < trafficSinks.size(); iterTrafficSink++){
      TrafficSink* currTrafficSink = trafficSinks[iterTrafficSink];
      cumulativeDelay = cumulativeDelay + (currTrafficSink->GetTotalPackets())*(currTrafficSink->GetAverageDelay());
      cumulativePackets = cumulativePackets + currTrafficSink->GetTotalPackets();
  }
  int droppedPackets = (Simulator::GetDroppedPackets()).size();
 // std::cout<<"Switch = "<< argv[1]<<" buffer = "<<maxBufferSize<<" total time = "<<timeInstants<<" trafficModel = "<<argv[4]<<" algorithm = "<<argv[5] <<std::endl;
  std::cout<<load << " "<< cumulativeDelay/cumulativePackets << " " << cumulativePackets <<" "<< droppedPackets << std::endl;          
  delete simulator;
      
}
 

