#ifndef TRAFFICSINK_H
#define TRAFFICSINK_H

#include "sink.h"
#include <vector>
#include "packet.h"

class TrafficSink:public Sink{

public:
   TrafficSink();
   ~TrafficSink();

   void SetTrafficSinkID(int trafficSinkID);
   int GetTrafficSinkID();
   std::vector<Packet*> GetBuffer();

   virtual void ReceivePacket();
   void AdvanceTrafficSink();
  
   double GetAverageDelay();
   int GetTotalPackets();
private:
   int d_trafficSinkID;
   std::vector<Packet*> d_buffer;
   int d_totalPackets;
   double d_averageDelay; 
};

#endif
