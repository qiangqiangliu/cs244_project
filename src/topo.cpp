#include "topo.h"

Topo::Topo(){
}

Topo::~Topo(){
}

void
Topo::SetTopoType(TopoType topoType){
   d_topoType = topoType;
}

Topo::TopoType
Topo::GetTopoType(){
   return d_topoType;
}
