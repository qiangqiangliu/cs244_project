
//#define N 55             //max number of vertices in one part
//#define INF 100000000    //just infinity
//
//int cost[N][N];          //cost matrix
//int n, max_match;        //n workers and n jobs
//int lx[N], ly[N];        //labels of X and Y parts
//int xy[N];               //xy[x] - vertex that is matched with x,
//int yx[N];               //yx[y] - vertex that is matched with y
//bool S[N], T[N];         //sets S and T in algorithm
//int slack[N];            //as in the algorithm description
//int slackx[N];           //slackx[y] such a vertex, that
//                         // l(slackx[y]) + l(y) - w(slackx[y],y) = slack[y]
//int prev[N];             //array for memorizing alternating paths

#include "hungarian.h"
#include <iostream>
#include <vector> 
#include <stdio.h>
#include <string.h>
#include "main.h"
 Hungarian::Hungarian(){
	
}

 Hungarian::~Hungarian(){
	
}
void  
Hungarian::SetValues(int x_n, std::vector < std::vector<int> > x_cost ){
     n=x_n;
     for (int i=0;i<n;i++){
	     for (int j=0;j<n;j++){
		     cost[i][j]= x_cost[i][j];
//#if DEBUG
//		     std::cout << "i =" << i <<"j= " << j <<"cost" << cost[i][j] << std::endl;
//
//#endif
	     }
     }
 // std::cout << " Leaving" << std::endl;
}
void 
Hungarian::InitLabels()
{
    memset(lx, 0, sizeof(lx));
    memset(ly, 0, sizeof(ly));
    for (int x = 0; x < n; x++)
        for (int y = 0; y < n; y++)
            lx[x] = Max(lx[x], cost[x][y]);

}

int 
Hungarian::Max(int x, int y){
	return (x>y?x:y);
}

int 
Hungarian::Min(int x, int y){
	return (x<y?x:y);
}

int Hungarian::MaxWeight()
{
    int ret = 0;                      //weight of the optimal matching
    max_match = 0;                    //number of vertices in current matching
 //   std::cout << "ret" ;
    memset(xy, -1, sizeof(xy));    
    memset(yx, -1, sizeof(yx));
    InitLabels();                    //step 0
    Augment();                        //steps 1-3
    for (int x = 0; x < n; x++){       //forMing answer there
        ret += cost[x][xy[x]];
#if DEBUG
    	std::cout << "  x  " << x  << "  y  " << xy[x] << std::endl;
#endif
    }
    return ret;
}

std::vector<int> 
Hungarian::ReturnY()
{
    std::vector <int> Y;
    for (int x = 0; x < n; x++){       //forMing answer there
         Y.push_back(xy[x]);
    }
    return Y;
}



void Hungarian::AddToTree(int x, int prevx) 
//x - current vertex,prevx - vertex from X before x in the alternating path,
//so we add edges (prevx, xy[x]), (xy[x], x)
{
    S[x] = true;                    //add x to S
    prev[x] = prevx;                //we need this when Augmenting
    for (int y = 0; y < n; y++)    //update slacks, because we add new vertex to S
        if (lx[x] + ly[y] - cost[x][y] < slack[y])
        {
            slack[y] = lx[x] + ly[y] - cost[x][y];
            slackx[y] = x;
        }
}


void Hungarian::UpdateLabels()
{
    int x, y, delta = INF;             //init delta as infinity
    for (y = 0; y < n; y++)            //calculate delta using slack
        if (!T[y])
            delta = Min(delta, slack[y]);
    for (x = 0; x < n; x++)            //update X labels
        if (S[x]) lx[x] -= delta;
    for (y = 0; y < n; y++)            //update Y labels
        if (T[y]) ly[y] += delta;
    for (y = 0; y < n; y++)            //update slack array
        if (!T[y])
            slack[y] -= delta;
}



void Hungarian::Augment()                         //main function of the algorithm
{
    if (max_match == n) return;        //check wether matching is already perfect
    int x, y, root;                    //just counters and root vertex
    int q[MAX_SIZE], wr = 0, rd = 0;          //q - queue for bfs, wr,rd - write and read
                                       //pos in queue
    memset(S, false, sizeof(S));       //init set S
    memset(T, false, sizeof(T));       //init set T
    memset(prev, -1, sizeof(prev));    //init set prev - for the alternating tree
    for (x = 0; x < n; x++)            //finding root of the tree
        if (xy[x] == -1)
        {
            q[wr++] = root = x;
            prev[x] = -2;
            S[x] = true;
            break;
        }

    for (y = 0; y < n; y++)            //initializing slack array
    {
        slack[y] = lx[root] + ly[y] - cost[root][y];
        slackx[y] = root;
    }
//second part of Augment() function
    while (true)                                                        //main cycle
    {
        while (rd < wr)                                                 //building tree with bfs cycle
        {
            x = q[rd++];                                                //current vertex from X part
            for (y = 0; y < n; y++)                                     //iterate through all edges in equality graph
                if (cost[x][y] == lx[x] + ly[y] &&  !T[y])
                {
                    if (yx[y] == -1) break;                             //an exposed vertex in Y found, so
                                                                        //Augmenting path exists!
                    T[y] = true;                                        //else just add y to T,
                    q[wr++] = yx[y];                                    //add vertex yx[y], which is matched
                                                                        //with y, to the queue
                    AddToTree(yx[y], x);                              //add edges (x,y) and (y,yx[y]) to the tree
                }
            if (y < n) break;                                           //Augmenting path found!
        }
        if (y < n) break;                                               //Augmenting path found!

        UpdateLabels();                                                //Augmenting path not found, so improve labeling
        wr = rd = 0;                
        for (y = 0; y < n; y++)        
        //in this cycle we add edges that were added to the equality graph as a
        //result of improving the labeling, we add edge (slackx[y], y) to the tree if
        //and only if !T[y] &&  slack[y] == 0, also with this edge we add another one
        //(y, yx[y]) or Augment the matching, if y was exposed
            if (!T[y] &&  slack[y] == 0)
            {
                if (yx[y] == -1)                                        //exposed vertex in Y found - Augmenting path exists!
                {
                    x = slackx[y];
                    break;
                }
                else
                {
                    T[y] = true;                                        //else just add y to T,
                    if (!S[yx[y]])    
                    {
                        q[wr++] = yx[y];                                //add vertex yx[y], which is matched with
                                                                        //y, to the queue
                        AddToTree(yx[y], slackx[y]);                  //and add edges (x,y) and (y,
                                                                        //yx[y]) to the tree
                    }
                }
            }
        if (y < n) break;                                               //Augmenting path found!
    }

    if (y < n)                                                          //we found Augmenting path!
    {
        max_match++;                                                    //increment matching
        //in this cycle we inverse edges along Augmenting path
        for (int cx = x, cy = y, ty; cx != -2; cx = prev[cx], cy = ty)
        {
            ty = xy[cx];
            yx[cy] = cx;
            xy[cx] = cy;
        }
        Augment();                                                      //recall function, go to step 1 of the algorithm
    }
}//end of Augment() function


 
