#include "arrival_model.h"

ArrivalModel::ArrivalModel(){
}
   
ArrivalModel::~ArrivalModel(){
}

ArrivalModel::ArrivalType 
ArrivalModel::GetArrivalType(){
   return d_arrivalType;
}

void 
ArrivalModel::SetArrivalType(ArrivalType arrivalType){
    d_arrivalType = arrivalType;
}
