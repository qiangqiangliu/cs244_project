#ifndef QUEUED_OUTPUT_PORT_H
#define QUEUED_OUTPUT_PORT_H


#include "packet.h"
#include <vector>
#include "output_port.h"

class QueuedOutputPort:public OutputPort{

public:
    QueuedOutputPort(int maxBufferSize);
    virtual ~QueuedOutputPort();

    std::vector<Packet*> GetBuffer();
    void AddPacketToBuffer(Packet* packet);

    virtual void SendPacket();
private:
    std::vector<Packet*> d_buffer;
    int d_maxBufferSize;
};

#endif
