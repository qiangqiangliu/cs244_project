#include "scheduling_input.h"

SchedulingInput::SchedulingInput(){
}   

SchedulingInput::~SchedulingInput(){
}

void
SchedulingInput::SetInputPortID(int inputPortID){
  d_inputPortID = inputPortID;
}

void
SchedulingInput::SetDesiredOutputPortIDs(std::vector<int> desiredOutputPortIDs){
   d_desiredOutputPortIDs = desiredOutputPortIDs;
}

int
SchedulingInput::GetInputPortID(){
   return d_inputPortID;
}

std::vector<int> 
SchedulingInput::GetDesiredOutputPortIDs(){
   return d_desiredOutputPortIDs;
}

void
SchedulingInput::MapSchedulingInput(std::vector<int> randomPerm){
   SetInputPortID(Map(randomPerm, GetInputPortID() - 1) + 1);
   std::vector<int> actualOutputPortIDs = GetDesiredOutputPortIDs();
   std::vector<int> mappedOutputPortIDs;
   for (int iter = 0; iter < actualOutputPortIDs.size(); iter++){
        mappedOutputPortIDs.push_back(Map(randomPerm, actualOutputPortIDs[iter] - 1) + 1);
   }
   SetDesiredOutputPortIDs(mappedOutputPortIDs);
}

int
SchedulingInput::Map(std::vector<int> randomPerm, int actualPort){
    return randomPerm[actualPort] ;
}
