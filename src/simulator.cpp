#include "simulator.h"

Network* Simulator::d_networkPtr = NULL;

int Simulator::d_pktNo = 0;

int Simulator::d_time = 0;

std::vector<int> Simulator::d_droppedPackets;

Simulator::Simulator(std::vector<ArrivalModel*> arrivalModels, std::vector<SchedulingAlgo*> schedulingAlgos, Switch::SwitchType switchType, Topo* topoPtr, int maxBufferSize){
   d_networkPtr = topoPtr->CreateNetwork(switchType, maxBufferSize);

   std::vector<TrafficGenerator*> trafficGenerators = d_networkPtr->GetTrafficGenerators();
   for (int iterTrafficGen = 0; iterTrafficGen < trafficGenerators.size(); iterTrafficGen++){
      trafficGenerators[iterTrafficGen]->SetArrivalModel(arrivalModels[iterTrafficGen]);
   }

   std::vector<Switch*> switches = d_networkPtr->GetSwitches();
   for (int iterSwitch = 0; iterSwitch < switches.size(); iterSwitch++){
      switches[iterSwitch]->SetSchedulingAlgo(schedulingAlgos[iterSwitch]);
   }
}

Simulator::~Simulator(){
   delete d_networkPtr;
}

int
Simulator::GetPacketNo(){
   d_pktNo++;
   return d_pktNo;
}

Network*
Simulator::GetNetworkPtr(){
   return d_networkPtr;
}

int
Simulator::GetTime(){
   return d_time;
}

void
Simulator::UpdateTime(){
    d_time++;
//   std::cout << d_time << std::endl;
}  

void
Simulator::AdvanceTimeInstant(){
   UpdateTime();

   std::vector<Switch*> switches = d_networkPtr->GetSwitches();
   for(int iterSwitch = 0; iterSwitch < switches.size(); iterSwitch++){
       switches[iterSwitch]->AdvanceSwitch();
   }

   std::vector<TrafficGenerator*> trafficGenerators = d_networkPtr->GetTrafficGenerators();
   for(int iterTrafficGenerator = 0; iterTrafficGenerator < trafficGenerators.size(); iterTrafficGenerator++){
       trafficGenerators[iterTrafficGenerator]->AdvanceTrafficGenerator();
   }

   std::vector<TrafficSink*> trafficSinks = d_networkPtr->GetTrafficSinks();
   for(int iterTrafficSink = 0; iterTrafficSink < trafficSinks.size(); iterTrafficSink++){
       trafficSinks[iterTrafficSink]->AdvanceTrafficSink();
   }

}

void
Simulator::DropPacket(int packetNo){
   d_droppedPackets.push_back(packetNo);
}

std::vector<int>
Simulator::GetDroppedPackets(){
   return d_droppedPackets;
}

/*   if (!ShouldWeGeneratePackets()){
       bool simulationLeft = false;
       std::vector<Switch*> switches = d_networkPtr->GetSwitches();
       for(int newIterSwitch = 0; newIterSwitch < switches.size(); newIterSwitch++){
           Switch* currentSwitch = switches[newIterSwitch];
           if (currentSwitch->CheckBackloggedPackets()) 
                simulationLeft = true;
       }
       return simulationLeft;
   }
   else
       return true;*/
