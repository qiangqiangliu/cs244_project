#include "queued_input_port.h"
#include "packet.h"
#include "link.h"
#include "output_port.h"
#include "network.h"
#include "simulator.h"
#include "traffic_sink.h"
#include <iostream>

QueuedInputPort::QueuedInputPort(int maxBufferSize){
   SetInputPortType(QUEUED_INPUT_PORT);
   d_maxBufferSize = maxBufferSize;
}

QueuedInputPort::~QueuedInputPort(){
}

std::vector<Packet*>
QueuedInputPort::GetBuffer(){
   return d_buffer;
}

std::vector<int>
QueuedInputPort::GetDesiredOutputPortIDs(){
   std::vector<int> desiredOutputPorts;
   std::vector<Packet*>::iterator iterBuffer;
   for (iterBuffer = d_buffer.begin(); iterBuffer != d_buffer.end(); iterBuffer++){
       int nextDesiredOutputPortID = (*iterBuffer)->GetNextOutputPortID();
       desiredOutputPorts.push_back(nextDesiredOutputPortID);
   }
   return desiredOutputPorts;
}

Packet*
QueuedInputPort::GetPacketForOutputPortID(int outputPortID){
   for (int packetPosition = 0; packetPosition < d_buffer.size(); packetPosition++){
      if(d_buffer[packetPosition]->GetNextOutputPortID() == outputPortID){
          Packet* desiredPacket = d_buffer[packetPosition];
          d_buffer.erase(d_buffer.begin() + packetPosition);
          return desiredPacket;
      }
   }
   return NULL;
} 

void
QueuedInputPort::ReceivePacket(){
   Packet* packet = GetAttachedLink()->ReadPacket();
   if (packet != NULL){
       if (d_buffer.size() < d_maxBufferSize){ 
          //std::cout<<"Got a Packet at input port "<<GetInputPortID()<<std::endl; 
          packet->UpdateHopCount();
          packet->SetNextOutputPortID();
          //Add it to the buffer
          d_buffer.push_back(packet);
       }
       else{
          Simulator::DropPacket(packet->GetPacketNo());
	  delete packet;
       } 
   }
   return;
}

SchedulingInput*
QueuedInputPort::GetSchedulingInput(){
   SchedulingInput* schedulingInput = new SchedulingInput(); 
   schedulingInput->SetInputPortID(GetInputPortID());
   schedulingInput->SetDesiredOutputPortIDs(GetDesiredOutputPortIDs());
   return schedulingInput;
}
